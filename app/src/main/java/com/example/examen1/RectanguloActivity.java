package com.example.examen1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RectanguloActivity extends AppCompatActivity {

    private TextView tvNombre;
    private EditText etBase;
    private EditText etAltura;
    private EditText etArea;
    private EditText etPerimetro;
    private Button btnCalcular;
    private Button btnRegresar;
    private Button btnLimpiar;
    private rectangulo rectangulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo);

        tvNombre = (TextView) findViewById(R.id.tvNombre);
        etBase = (EditText) findViewById(R.id.etBase);
        etAltura = (EditText) findViewById(R.id.etAltura);
        etArea = (EditText) findViewById(R.id.etArea);
        etPerimetro = (EditText) findViewById(R.id.etPerimetro);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);

        final String nombre = getIntent().getStringExtra("nombre");
        tvNombre.setText("Mi nombre es " + nombre);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etBase.getText().toString().matches("")){
                    Toast.makeText(RectanguloActivity.this, "Escribe la base el rectángulo", Toast.LENGTH_SHORT).show();
                } else if (etAltura.getText().toString().matches("")){
                    Toast.makeText(RectanguloActivity.this, "Escribe la altura el rectángulo", Toast.LENGTH_SHORT).show();
                } else {
                    rectangulo = new rectangulo(Integer.parseInt(etBase.getText().toString()), Integer.parseInt(etAltura.getText().toString()));
                    etArea.setText(String.valueOf(rectangulo.calcularArea()));
                    etPerimetro.setText(String.valueOf(rectangulo.calcularPerimetro()));
                }
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etBase.setText("");
                etAltura.setText("");
                etArea.setText("");
                etPerimetro.setText("");
            }
        });
    }
}
