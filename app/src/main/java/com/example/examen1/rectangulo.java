package com.example.examen1;

public class rectangulo {

    private int base;
    private int altura;

    public rectangulo() {
    }

    public rectangulo(int base, int altura) {
        this.base = base;
        this.altura = altura;
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public float calcularArea(){
        return this.base * this.altura;
    }

    public float calcularPerimetro(){
        return this.base * 2 + this.altura * 2;
    }

}
